import './App.css';
import React, { useState, useEffect } from 'react';
import { WiDaySunny } from 'weather-icons-react';







function App() {

  const [info, setInfo] = useState();
  const [search, setSearch] = useState("Mumbai");
  const [error, setError] = useState(null);
  
 
  
  
  
  useEffect(() => {
    
    
    const fetchdata = async () => {
      const apiURL = `https://api.openweathermap.org/data/2.5/weather?q=${search}&units=metric&appid=093b18f111a7e309e4efc31a005eb46b`;
      fetch(apiURL)
        .then(res => {
          if (!res.ok)
          {
            throw Error('City not Found');
            }
          return res.json();
        })
        .then(data => {
          setInfo(data);
          
          
          
          
          
        })
        .catch(err => {
          setError(err.message);
          setInfo(false);
      })
    }
    fetchdata();
  }, [search]);

     
      
 
  
  
  return (
    <>
   <div>
        <div className="box1">
          <div className="sun"><WiDaySunny size={290} color='#FFFFFF' /></div>
      </div>
      <div className="box">
        <div>  <input className="inputF"  type="search" value={search} onChange={(event) => {
            setSearch(event.target.value)
          }} /></div>
        
            {!info ? (error && <div className="temp"><h1>{error}</h1></div>) :
            <div>
            <div className="cityName"><h1>{search}({info.sys.country})</h1></div>

            <div className="temp"><h1>{info.main.temp}<span>&#8451;</span></h1></div>
            <div className="temp"><h2>Min:{info.main.temp_min}<span>&#8451;</span>/Max:{info.main.temp_max}<span>&#8451;</span></h2></div>
            <div className="temp">{info.weather.map(item => (<h2>{item.main}</h2>))}</div>
            <div className="temp"><h2>Wind:{info.wind.speed}km/h</h2></div>
           </div>
            }
            
        

       
        </div>
        </div>
      </>
  );
}

export default App;
